### Ansible Adhoc-Commands 

These are commands you run on the fly, thatallow you to interact with your infrastructure. 

This is particularly useful when you want to query your infrastructure. These are usually single commands . 

The most common is used to check connectivity.

```ansible all -m ping```

Syntax:

``` ansible <name_of_group> -m <ansible_module>```

One very useful ansible module is the shell, which allows you to run a shell. 

```ansible all -m shell -a "uptime"```

***check free space***

***check environment variable***